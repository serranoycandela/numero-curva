
p_usv = "/home/fidel/GitLab/numero-curva/data/usv.shp"
capa_usv = QgsVectorLayer(p_usv,"usv","ogr")

p_suelo = "/home/fidel/GitLab/numero-curva/data/suelos_region_utm.shp"
capa_suelo = QgsVectorLayer(p_suelo,"suelo","ogr")

p_usv_suelos = "/home/fidel/GitLab/numero-curva/data/usv_suelos.shp"

params = {"A": capa_suelo,
          "B": capa_usv,
          "SPLIT": True,
          "RESULT": p_usv_suelos
}
processing.run("sagang:intersect",params)