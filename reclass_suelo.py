import csv

p_la_capa = "/home/fidel/GitLab/numero-curva/data/suelos_region.shp"
la_capa = QgsVectorLayer(p_la_capa,"suelos","ogr")

#agregar la columna "tipo"
if 'tipo' not in la_capa.fields().names():
    la_capa.dataProvider().addAttributes([QgsField('tipo', QVariant.String)])
    la_capa.updateFields()
    
    
tipo_hidro = {}

with open('/home/fidel/GitLab/numero-curva/tipo_hidro_suelos.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        tipo_hidro[row[0]] = {"Gruesa": row[1], "Media": row[2], "Fina": row[3]}
    
    

la_capa.startEditing()

for f in la_capa.getFeatures(): # recorrer los elementos de la capa
    suelo1 = f['NOM_SUE1']
    textura = f['CLA_TEX']
    if not suelo1: # si es cuerpo de agua ponle A 
        f['tipo'] = "A"
    else:
        f['tipo'] = tipo_hidro[suelo1][textura]
    la_capa.updateFeature(f)
la_capa.commitChanges()