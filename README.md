# Número de curva

El número de curva de escorrentía (también llamado número de curva o simplemente CN) es un parámetro empírico utilizado en hidrología para predecir la escorrentía directa o la infiltración del exceso de lluvia. El método del número de curva fue desarrollado por el Servicio de Conservación de Recursos Naturales del USDA, que anteriormente se llamaba Servicio de Conservación de Suelos o SCS, el número todavía se conoce popularmente como "número de curva de escorrentía SCS" en la literatura. El número de la curva de escorrentía se desarrolló a partir de un análisis empírico de la escorrentía de pequeñas cuencas y parcelas de ladera monitoreadas por el USDA. Es ampliamente utilizado y es un método eficiente para determinar la cantidad aproximada de escorrentía directa de un evento de lluvia en un área en particular.


## Los códigos

Este repositorio contiene códigos escritos en python para ejecutarse dentro de QGIS 3 para obtener un raster con los números de curva a partir de información pública en México. El procedimiento en términos generales consiste en reclasificar la capa de uso de suelo y vegetación de INEGI, reclasificar la capa de tipos de suelos también de INEGI, intersectar las capas reclasificadas, asignar el número de curva a cada intersección, y finalmente exportar un raster resultante. Los códigos que implementan este procedimiento son los siguientes:

- reclass_usv.py          (reclasifica USV serie 7 de INEGI)
- reclass_suelo.py        (reclasifica la capa de Edafología de INEGI)
- intersect_usv_suelos.py (intersecta la capas reclasificadas)
- asigna_numero_curva.py  (asigna número de curva y exporta el resultado)

El procedimiento requiere dos capas vectoriales y dos tablas en formato csv. Las capas vectoriales son la de uso de suelo y vegetación y la de Edafología, ambas de INEGI. Y las tablas en formato csv contienen la información para asignar el grupo higrológico en función del tipo de suelo y la textura del mismo, y para asignar el número de curva en función de la cobertura y el grupo hidrológico.


## Ejemplo

El área de estudio para la que se aplicó este procedimiento es bla bla:

<img src="region_estudio.png" width="600px">

Paso 1. Reclasificar la capa de uso de suelo y vegetación.

El objetivo es agrupar categorías de uso de suelo y vegetación en las siguientes categorías generales: Agricultura, Bosque, Cuerpo de agua, Infraestructura, Matorral, Pastizal, Vegetacion secundaria, Zona urbana, Selva baja, Suelo desnudo. El siguiente mapa muestra el resultado de correr el script reclass_usv.py para ese proposito.

<img src="cobertura.png" width="600px">

Las categorías de uso de suelo y vegetación quedaron agrupadas de la siguiete manera:

Suelo desnudo: DESPROVISTO DE VEGETACIÓN, SIN VEGETACIÓN APARENTE

Zona urbana: ASENTAMIENTOS HUMANOS

Bosque: BOSQUE DE OYAMEL, BOSQUE CULTIVADO, BOSQUE DE TÁSCATE, BOSQUE MESÓFILO DE MONTAÑA, BOSQUE DE PINO, BOSQUE DE PINO-ENCINO, BOSQUE DE ENCINO, BOSQUE DE ENCINO-PINO, VEGETACIÓN HALÓFILA HIDRÓFILA, TULAR

Cuerpo de agua: CUERPO DE AGUA

Agricultura: AGRICULTURA DE HUMEDAD ANUAL, AGRICULTURA DE RIEGO ANUAL, AGRICULTURA DE RIEGO ANUAL Y PERMANENTE, AGRICULTURA DE RIEGO ANUAL Y SEMIPERMANENTE, AGRICULTURA DE RIEGO PERMANENTE, AGRICULTURA DE RIEGO SEMIPERMANENTE, AGRICULTURA DE TEMPORAL ANUAL, AGRICULTURA DE TEMPORAL ANUAL Y PERMANENTE, AGRICULTURA DE TEMPORAL ANUAL Y SEMIPERMANENTE, AGRICULTURA DE TEMPORAL PERMANENTE, AGRICULTURA DE TEMPORAL SEMIPERMANENTE, AGRICULTURA DE TEMPORAL SEMIPERMANENTE Y PERMANENTE

Matorral: MATORRAL CRASICAULE, MATORRAL DESÉRTICO ROSETÓFILO, MATORRAL SARCOCAULE

Pastizal: PASTIZAL CULTIVADO, PASTIZAL HALÓFILO, PASTIZAL INDUCIDO, PRADERA DE ALTA MONTAÑA

Selva baja: SELVA BAJA CADUCIFOLIA, PALMAR INDUCIDO

Vegetacion secundaria: VEGETACION SECUNDARIA ARBUSTIVA DE BOSQUE DE OYAMEL, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE DE OYAMEL, VEGETACIÓN SECUNDARIA ARBUSTIVA DE BOSQUE DE TÁSCATE, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE DE TÁSCATE, VEGETACIÓN SECUNDARIA ARBUSTIVA DE BOSQUE MESÓFILO DE MONTAÑA, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE MESÓFILO DE MONTAÑA, VEGETACIÓN SECUNDARIA ARBUSTIVA DE BOSQUE DE PINO, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE DE PINO, VEGETACIÓN SECUNDARIA ARBUSTIVA DE BOSQUE DE PINO-ENCINO, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE DE PINO-ENCINO, VEGETACIÓN SECUNDARIA ARBUSTIVA DE BOSQUE DE ENCINO, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE DE ENCINO, VEGETACIÓN SECUNDARIA ARBUSTIVA DE BOSQUE DE ENCINO-PINO, VEGETACIÓN SECUNDARIA ARBÓREA DE BOSQUE DE ENCINO-PINO, VEGETACIÓN SECUNDARIA ARBUSTIVA DE MATORRAL CRASICAULE, VEGETACIÓN SECUNDARIA ARBUSTIVA DE SELVA BAJA CADUCIFOLIA, VEGETACIÓN SECUNDARIA ARBÓREA DE SELVA BAJA CADUCIFOLIA, VEGETACIÓN SECUNDARIA HERBÁCEA DE BOSQUE DE ENCINO



Paso 2. Reclasificar la capa de edafología.

El grupo hidrológico depende del tipo de suelo y de la textura del mismo. La siguiete tabla muestra esa dependencia y es el insumo del script reclass_suelo.py.

Tabla 1.
| Tipo de suelo | Gruesa | Media | Fina |
| ------------- | ------ | ----- | ---- |
| Vertisol | B | C | D |
| Luvisol | B | C | D |
| Cambisol | B | C | D |
| Feozem | A | B | C |
| Regosol | A | B | C |
| Andosol | A | B | C |
| Litosol | A | B | C |
| Rendzina | A | B | C |
| Solonchak | B | C | D |
| Fluvisol | A | B | C |
| Castañozem | A | B | C |

El siguiente mapa muestra el resultado de correr el script reclass_suelo.py usando la tabla 1 como insumo.


<img src="suelo.png" width="600px">

Paso 3. Intersectar las capas reclasificadas.

Paso 4. Asignar números de curva.

El número de curva depende del la cobertura y el tipo de suelo. La siguiente tabla muestra los valores de número de curva para cada tipo de cobertura y para cada grupo hidrológico de suelo, presentes en el área de estudio.

Tabla 2.
| Tipo de Cobertura | A | B | C | D |
| ----------------- | - | - | - | - |
| Agricultura | 64 | 75 | 82 | 85
| Bosque | 36 | 60 | 73 | 79 |
| Cuerpo de agua | 100 | 100 | 100 | 100 |
| Infraestructura | 95 | 96 | 97 | 97 |
| Matorral | 35 | 56 | 70 | 77 |
| Pastizal | 49 | 69 | 79 | 84 |
| Vegetacion secundaria | 45 | 66 | 77 | 83 |
| Zona urbana | 95 | 96 | 97 | 97 | 
| Selva baja | 26 | 40 | 58 | 61 |
| Suelo desnudo | 77 | 86 | 91 | 94 |


El siguiente mapa muestra el resultado de correr el script asigna_numero_curva.py usando la tabla 2 como insumo.

<img src="nc.png" width="600px">





## Autores
M. en G. Fidel Serrano Candela, Laboratorio Nacional de Ciencias de la Sostenibilidad, 
Instituto de Ecología, Universidad Nacional Autónoma de México.

M. en C. Aymara Olin Ramírez González, Universidad Autónoma de México.


## License
### GNU GPL v3
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)    


